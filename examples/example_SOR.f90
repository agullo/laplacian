program main
use parameters
use grid_n_operators
use MPI_routines
use mod_inv_lap
implicit none

uni_grid = .False.

Read_ext_data = .FALSE.

LZn = 1       !box length 
R1n = 5       !box outher radius 
R0n = 4       !box inner radius (CANNOT BE ZERO !)
LRn = 1       !box length 
NR = 100   ! number of points of the radius
NZ = 100  ! number of points of the high


tol_SOR = 1.00e-2!tolerance for SOR inversion
dt_SOR = 1.0     !step size for SOR inversion


! we start MPI routines, set box splits (mpion.f90 or mpioff.f90)
! the box is splitted along the axis. Each processor have a slice of the column to deal with.
call mpi_initisialisation
	IF(Read_ext_data)then
		call read_diag_w_lapw(1) ! get Nr and Nz and box sizes. overwritting the data line 12-17
	Endif
  call de_allocate(2)

  call adapted_grid(0)
  call mpi_grid
  call adapted_grid(1)
  ! we allocate the arrays. (parameter.f90)
  call de_allocate(1)

  ! we set all arrays to zero. (parameter.f90)
  call de_allocate(0)
  call CB_BT_mpi_ALLfield(0) !initialisation
  call CB_RL_mpi_ALLfield(0) !initialisation
 call tests_operators

 call de_allocate(-1)
 call de_allocate(-2)
 call MPI_final

 contains
 !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine tests_operators
use parameters
use grid_n_operators
implicit none
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext) :: f
real*8,     dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext)  :: drf, dzf, lapf
character(len=12) :: file_name


if(Read_ext_data)then

  call read_diag_w_lapw(3)
  
	if(myid==0)then
	write(file_name , '(A12)') 'test_gri.txt'
	open(299, file=file_name, status = 'new', form = 'formatted')
	write(299,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") r(1:NR)
	write(299,FMT= "("//trim(adjustl(NZ_string))//"(E16.8))") z(1:NZ)
	close(299)
	endif

	 call gather_field(w)
	if(myid==0)then 
	write(file_name , '(A12)') 'guess__w.txt'
	open(300, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(300,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(300)
	endif  

	call SOR_inv_lap

	 call gather_field(w)
	 if(myid==0)then  
	write(file_name , '(A12)') 'next___w.txt'
	open(304, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(304,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(304)
	endif


else


	do ir = Sr_ext,Er_ext
	do iz = Sz_ext,Ez_ext
		   f(ir,iz) = exp(-(r(ir)-(LRn)/2-R0n)**2. -16.*(z(iz)-LZn/2)**2.)
	enddo
	enddo

		  call laplacian(f,lapf)
		  call Rgradient(f,drf)
		  call Zgradient(f,dzf)



		  w =0.
		  lapw = lapf
		      
		  call SOR_inv_lap



	write(NR_string,'(I8)') NR
	write(NZ_string,'(I8)') NZ
	if(myid==0)then
	write(file_name , '(A12)') 'test_gri.txt'
	open(299, file=file_name, status = 'new', form = 'formatted')
	write(299,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") r(1:NR)
	write(299,FMT= "("//trim(adjustl(NZ_string))//"(E16.8))") z(1:NZ)
	close(299)
	endif

	 call gather_field(f)
	if(myid==0)then 
	write(file_name , '(A12)') 'test____.txt'
	open(300, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(300,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(300)
	endif

	 call gather_field(drf)
	if(myid==0)then  
	write(file_name , '(A12)') 'test__dr.txt'
	open(301, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(301,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(301)
	endif

	 call gather_field(dzf)
	 if(myid==0)then  
	write(file_name , '(A12)') 'test__dz.txt'
	open(302, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(302,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(302)
	endif

	 call gather_field(lapf)
	 if(myid==0)then  
	write(file_name , '(A12)') 'test_lap.txt'
	open(303, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(303,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(303)
	endif

	 call gather_field(w)
	 if(myid==0)then  
	write(file_name , '(A12)') 'testIlap.txt'
	open(304, file=file_name, status = 'new', form = 'formatted')
	do iz = 1,NZ
		   write(304,FMT= "("//trim(adjustl(NR_string))//"(E16.8))") fullfield(:,iz)
	enddo
	close(304)
	endif

	  
	     
endif


end subroutine tests_operators


end program main
