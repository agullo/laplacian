module mod_inv_lap

contains
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine SOR_inv_lap
use parameters
use mpi_routines
use grid_n_operators
implicit none
real*8 :: error, error_b, error_all
logical :: stayrunning_SOR

error = 0.
iter_SOR = 1
stayrunning_SOR = .true.
error_b = 1.


do while (stayrunning_SOR)


  IF(para == 'Z')THEN
    call CB_BT_mpi_ALLfield(1,w)
  ENDIF
  IF(para == 'R')THEN
    call CB_RL_mpi_ALLfield(1,w)
  ENDIF
  call CB_box_w

  w_RHS =0.

  do iz = Sz,Ez_p
    do ir= Sr,Er_p 
            w_RHS(ir,iz) =   (nr2l(ir) - nr1(ir)*ri(ir))*w(ir+1,iz) &
                             + (mr2l(ir) - mr1(ir)*ri(ir))*w(ir-1,iz) &
                             + pr2l(ir)*w(ir+2,iz)  &                             
                             + mz2l(iz)*w(ir,iz-1)  &
                             + nz2l(iz)*w(ir,iz+1)  &
                             + pz2l(iz)*w(ir,iz+2)  &
                             - lapw(ir,iz)
            error =  error + point_volume(ir,iz)*abs(w_RHS(ir,iz) + (-sr1(ir)*ri(ir)+sr2l(ir) +sz2l(iz))*w(ir,iz) )  
            w(ir,iz)     = (w(ir,iz) + point_volume(ir,iz)*dt_SOR*w_RHS(ir,iz))/&
                           (1. +point_volume(ir,iz)*dt_SOR *(sr1(ir)*ri(ir)-sr2l(ir) -sz2l(iz))) 
    enddo
  enddo

  if(.not.(para == 'R' .and. MYID < NUMPROCS-1) )then
    do iz=Sz,Ez_p
            w_RHS(NR,iz) =   (nr2r - nr1(NR)*ri(NR))*w(NR+1,iz) &
                             + (mr2r - mr1(NR)*ri(NR))*w(NR-1,iz) &
                             + pr2r*w(NR-2,iz)  &                             
                             + mz2l(iz)*w(NR,iz-1)  &
                             + nz2l(iz)*w(NR,iz+1)  &
                             + pz2l(iz)*w(NR,iz+2)  &
                             - lapw(NR,iz)
            error =  error + point_volume(NR,iz)*abs(w_RHS(NR,iz) + (-sr1(NR)*ri(NR)+sr2r+sz2l(iz))*w(NR,iz) )   
            w(NR,iz)     = (w(NR,iz) + point_volume(NR,iz)*dt_SOR *w_RHS(NR,iz))/&
                           (1. +point_volume(NR,iz)*dt_SOR*(sr1(NR)*ri(NR)-sr2r-sz2l(iz)) )  
    enddo
  endif


  if( .not.(para == 'Z' .and. MYID < NUMPROCS-1))then
    do ir= Sr,Er_p 
            w_RHS(ir,Nz) =   (nr2l(ir) - nr1(ir)*ri(ir))*w(ir+1,Nz) &
                             + (mr2l(ir) - mr1(ir)*ri(ir))*w(ir-1,Nz) &
                             + pr2l(ir)*w(ir+2,Nz)  &                             
                             + mz2r*w(ir,Nz-1)  &
                             + nz2r*w(ir,Nz+1)  &
                             + pz2r*w(ir,Nz-2)  &
                             - lapw(ir,nz)
            error =  error + point_volume(ir,Nz)*abs(w_RHS(ir,Nz) + (-sr1(ir)*ri(ir)+sr2l(ir)+sz2r)*w(ir,Nz) )   
            w(ir,Nz)     = (w(ir,Nz) + point_volume(ir,Nz)*dt_SOR*w_RHS(ir,Nz))/&
                           (1.+point_volume(ir,Nz)*dt_SOR*(sr1(ir)*ri(ir)-sr2l(ir)-sz2r))    
    enddo
  endif

  if(MYID == NUMPROCS - 1)then
            w_RHS(NR,NZ) =   (nr2r - nr1(NR)*ri(NR))*w(NR+1,NZ) &
                             + (mr2r - mr1(NR)*ri(NR))*w(NR-1,NZ) &
                             + pr2r*w(NR-2,NZ)  &                             
                             + mz2r*w(NR,NZ-1)  &
                             + nz2r*w(NR,NZ+1)  &
                             + pz2r*w(NR,NZ-2)  &
                             - lapw(NR,NZ)
            error =  error + point_volume(NR,NZ)*abs(w_RHS(NR,NZ) + (-sr1(NR)*ri(NR)+sr2r+sz2r)*w(NR,NZ))   
            w(NR,NZ)     = (w(NR,NZ) +point_volume(NR,NZ)*dt_SOR* w_RHS(NR,NZ))/&
                           (1.+point_volume(NR,NZ)*dt_SOR*(sr1(NR)*ri(NR)-sr2r-sz2r))  
  endif





  if(isnan(error))then
   print*, 'SOR failed : NaN error'
   print*, myid, iter_SOR
   stop 0
  endif

  call reducesum(error,error_all)

!if(MYID ==0)then
 print*, MYID, iter_SOR, error, error_all
!endif

  if(abs((error_all - error_b)/error_b)  < tol_SOR .and. error_all < error_b)then
    stayrunning_SOR = .false.
    if(MYID==0)then   
      print*, "tolerance reached for SOR.", iter_SOR
    endif
  endif

  if(iter_SOR> 1000000 )then
    stayrunning_SOR = .false.
    if(MYID==0)then
    print*, "itermax reached for SOR.",error_all,tol_SOR
    endif
  endif
  
  iter_SOR = iter_SOR+1
  error_b = error_all
  error_all = 0.
  error = 0.


enddo






end subroutine SOR_inv_lap

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end module mod_inv_lap

