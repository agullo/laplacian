module mpi_routines

contains

subroutine mpi_initisialisation
use parameters
implicit none

 MYID=0
 NUMPROCS=1

end subroutine mpi_initisialisation
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine mpi_grid
use parameters
implicit none
 para = 'Z'
    Sr = 1
    Er = NR
    Sr_ext = 0
    Er_ext = NR+1
    Er_p = Er-1
    Sz = 1
    Ez = NZ
    Sz_ext = 0
    Ez_ext = NZ+1
    Ez_p = Ez-1
MYID = 0

end subroutine mpi_grid
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine reducesum(a_in,a_out)
use parameters
implicit none
real*8, intent(in) :: a_in
real*8, intent(out) :: a_out          
a_out = a_in
end subroutine reducesum
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine reducemax(a_in,a_out)
use parameters
implicit none
real*8, intent(in) :: a_in
real*8, intent(out) :: a_out   
a_out = a_in
end subroutine reducemax
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_logical(a) 
use parameters
implicit none
logical, intent(inout) :: a

end subroutine bcast_logical
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_int(a) 
use parameters
implicit none
integer, intent(inout) :: a

end subroutine bcast_int
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine bcast_real(a) 
use parameters
implicit none
real*8, intent(inout) :: a

end subroutine bcast_real
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_BT_mpi_ALLfield(i,field1, field2, field3, field4, field5, field6)
use parameters
implicit none
integer*4, intent(in) :: i
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext),optional, intent(inout) :: field1, field2, field3, field4, field5, field6
 
end subroutine CB_BT_mpi_ALLfield
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine CB_RL_mpi_ALLfield(i,field1, field2, field3, field4, field5, field6)
use parameters
implicit none
integer*4, intent(in) :: i
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext),optional, intent(inout) :: field1, field2, field3, field4, field5, field6
 
end subroutine CB_RL_mpi_ALLfield
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine gather_field(field)
use parameters
implicit none
real*8, dimension(Sr_ext:Er_ext,Sz_ext:Ez_ext), intent(in) :: field

fullfield(1:NR,1:NZ) = field(1:NR,1:NZ)

end subroutine gather_field
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine wait_all_here
implicit none
 
end subroutine wait_all_here
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine MPI_final
implicit none
 
end subroutine MPI_final

end module mpi_routines
