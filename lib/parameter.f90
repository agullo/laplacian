!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%% parameters list module %%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
module parameters
implicit none


!MPI n grid parameters
integer*4 :: NR, NZ
integer*4 :: Sz,Ez,Sr,Er,Ez_p,Er_p
integer*4 :: Sz_ext, Ez_ext, Sr_ext, Er_ext
integer*4 :: MYID, NUMPROCS,IERR
character*1 :: para
integer*4 :: iz, ir

!output parameters
character(len=22) :: NR_string,NZ_string
real*8, allocatable, dimension(:,:) :: fullfield

!equation parameters
!real*8 :: Ha, Re, Rem, Rn, VA, LN
!real*8 :: LZ, LR, dr, dz, R0, R1
real*8 :: LZn, LRn, R1n, R0n
!real*8 :: Jnorm
real*8 :: dt, dtn, dtn_vol


!field variables
real*8, allocatable, dimension(:,:) :: w, lapw, w_RHS

logical :: uni_grid, Read_ext_data
real*8, allocatable, dimension(:,:) :: point_volume
real*8, allocatable, dimension(:) :: r, z, ri, ri2, r_,z_
real*8, allocatable, dimension(:) :: rp2,rp1,rm1,rm2,zp2,zp1,zm2,zm1
real*8, allocatable, dimension(:) :: mz1, nz1, sz1, mr1, nr1, sr1
real*8 :: mz2r, nz2r, sz2r, pz2r, mr2r, nr2r, sr2r, pr2r
real*8, allocatable, dimension(:) :: mz2l, nz2l, sz2l, pz2l, mr2l, nr2l, sr2l, pr2l



!communication buffers
real*8, allocatable, dimension(:)  :: bufferLeft_all, bufferRight_all,SendLeft_all,SendRight_all
real*8, allocatable, dimension(:)  :: bufferBOT_all, bufferTOP_all,SendBOT_all,SendTop_all
integer*4, allocatable, dimension(:)  :: transfert_status_RL, transfert_status_BT
integer*4, allocatable, dimension(:,:)  :: status_array_RL, status_array_BT

!SOR parameters
real*8 ::  tol_SOR, dt_SOR
integer*8 :: iter_SOR

contains
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine de_allocate(i)

implicit none
integer :: i

if(i==1) then

allocate(w(Sr_ext:Er_ext,Sz_ext:Ez_ext), lapw(Sr_ext:Er_ext,Sz_ext:Ez_ext),w_RHS(Sr_ext:Er_ext,Sz_ext:Ez_ext))


elseif(i==2)then

allocate(fullfield(1:NR,1:NZ))

allocate(ri(0:NR+1),ri2(0:NR+1),r(0:NR+1),z(0:NZ+1),r_(0:NR+1),z_(0:NZ+1) )

allocate(point_volume(1:NR,1:NZ))
allocate(rp2(1:NR-1),rp1(1:NR),rm1(1:NR),rm2(2:NR),zp2(1:NZ-1),zp1(1:NZ),zm2(2:NZ),zm1(1:NZ))
allocate(mz1(1:NZ), nz1(1:NZ), sz1(1:NZ), mr1(1:NR), nr1(1:NR), sr1(1:NR))
allocate(mz2l(1:NZ-1), nz2l(1:NZ-1), sz2l(1:NZ-1), pz2l(1:NZ-1), mr2l(1:NR-1), nr2l(1:NR-1), sr2l(1:NR-1), pr2l(1:NR-1))

elseif(i==0)then
w = 0.
lapw = 0.
w_RHS = 0.
fullfield=0.

elseif(i==-1)then

deallocate(w, lapw,w_RHS)

elseif(i==-2)then

deallocate(r, z, ri, ri2,r_,z_)
deallocate(point_volume)
deallocate(rp2,rp1,rm1,rm2,zp2,zp1,zm2,zm1)
deallocate(mz1, nz1, sz1, mr1, nr1, sr1)
deallocate(mz2l, nz2l, sz2l, pz2l, mr2l, nr2l, sr2l, pr2l)

deallocate(fullfield)

endif
end subroutine de_allocate

end module parameters
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
